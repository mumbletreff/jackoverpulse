package com.gitlab.groups.mumbletreff.tools.jackoverpulse;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;

public class Main {
	protected static final String VERSION = "0.1";
	public static boolean isDebianBased,pactlAndCmdAreAv,jackdIsAv,qjackctlIsAv,pavucontrolIsAv;
	public static String PID;
	public static final int PROCESS_TOCHECK_PACTL_AND_CMD=1;
	public static final int PROCESS_TOCHECK_JACKD=2;
	public static final int PROCESS_TOCHECK_QJACKCTL=3;
	public static final int PROCESS_TOCHECK_PAVUCONTROL=4;
	public static boolean checkprocessavibility(int process){
		boolean isAvivable=false;
		if (process==PROCESS_TOCHECK_PACTL_AND_CMD){
			int score=0;//to success this check the score has to be 6 at least
			try {
			Runtime rt = Runtime.getRuntime();
			String[] commands = {"pacmd","--version"};
			System.out.println("Running "+"pacmd --version"+":\n");

			Process proc;
				proc = rt.exec(commands);


			BufferedReader stdInput = new BufferedReader(new 
			     InputStreamReader(proc.getInputStream()));

			BufferedReader stdError = new BufferedReader(new 
			     InputStreamReader(proc.getErrorStream()));

			// read the output from the command
			System.out.println("Here is the standard output of the command:\n");
			String s = null;
			//int state =0;
			while ((s = stdInput.readLine()) != null) {
			    System.out.println(s);
			    if(s.contains("pacmd")){
			    	score++;
			    }else if(s.contains("libpulse")){
			    	score++;
			    }
			    
			}

			// read any errors from the attempted command
			System.out.println("Here is the standard error of the command (if any):\n");
			while ((s = stdError.readLine()) != null) {
			    System.out.println(s);
			}
			
			
			
			//Runtime rt = Runtime.getRuntime();
			commands[0] = "pactl";
			System.out.println("Running "+"pacmd --version"+":\n");
			proc = rt.exec(commands);


		stdInput = new BufferedReader(new 
		     InputStreamReader(proc.getInputStream()));

		stdError = new BufferedReader(new 
		     InputStreamReader(proc.getErrorStream()));

			// read the output from the command
			System.out.println("Here is the standard output of the command:\n");
			s = null;
			//int state =0;
			while ((s = stdInput.readLine()) != null) {
			    System.out.println(s);
			    if(s.contains("pactl")){
			    	score++;
			    }else if(s.contains("libpulse")){
			    	score++;
			    }
			    
			}

			// read any errors from the attempted command
			System.out.println("Here is the standard error of the command (if any):\n");
			while ((s = stdError.readLine()) != null) {
			    System.out.println(s);
			}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//System.out.println("score="+score);
			if (score>=6)isAvivable=true;
		}	
		
		if (process == PROCESS_TOCHECK_JACKD) {
			int score = 0;// to success this check the score has to be 3 at
							// least
			try {
				Runtime rt = Runtime.getRuntime();
				String[] commands = { "jackd", "--version" };
				System.out.println("Running " + "jackd --version" + ":\n");

				Process proc;
				proc = rt.exec(commands);

				BufferedReader stdInput = new BufferedReader(
						new InputStreamReader(proc.getInputStream()));

				BufferedReader stdError = new BufferedReader(
						new InputStreamReader(proc.getErrorStream()));

				// read the output from the command
				System.out
						.println("Here is the standard output of the command:\n");
				String s = null;
				// int state =0;
				while ((s = stdInput.readLine()) != null) {
					System.out.println(s);
					if (s.contains("jackdmp")) {
						score++;
					}

				}

				// read any errors from the attempted command
				System.out
						.println("Here is the standard error of the command (if any):\n");
				while ((s = stdError.readLine()) != null) {
					System.out.println(s);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (score >= 3)
				isAvivable = true;
		}
		
		if (process == PROCESS_TOCHECK_PAVUCONTROL) {
			int score = 0;// to success this check the score has to be 2 at least
			try {
				Runtime rt = Runtime.getRuntime();
				String[] commands = { "pavucontrol", "-h" };
				System.out.println("Running " + "pavucontrol -h" + ":\n");

				Process proc;
				proc = rt.exec(commands);

				BufferedReader stdInput = new BufferedReader(
						new InputStreamReader(proc.getInputStream()));

				BufferedReader stdError = new BufferedReader(
						new InputStreamReader(proc.getErrorStream()));

				// read the output from the command
				System.out
						.println("Here is the standard output of the command:\n");
				String s = null;
				// int state =0;
				while ((s = stdInput.readLine()) != null) {
					System.out.println(s);
					if (s.contains("pavucontrol")) {
						score++;
					}else if (s.contains("PulseAudio")) {
						score++;
					}else if (s.contains("Usage")) {
						score++;
					}

				}

				// read any errors from the attempted command
				System.out
						.println("Here is the standard error of the command (if any):\n");
				while ((s = stdError.readLine()) != null) {
					System.out.println(s);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (score >= 2)
				isAvivable = true;
		}
		
		if (process == PROCESS_TOCHECK_QJACKCTL) {
			int score = 0;// to success this check the score has to be 1 at least
			try {
				Runtime rt = Runtime.getRuntime();
				String[] commands = { "qjackctl", "-v" };
				System.out.println("Running " + "qjackctl -v" + ":\n");

				Process proc;
				proc = rt.exec(commands);

				BufferedReader stdInput = new BufferedReader(
						new InputStreamReader(proc.getInputStream()));

				BufferedReader stdError = new BufferedReader(
						new InputStreamReader(proc.getErrorStream()));

				// read the output from the command
				System.out
						.println("Here is the standard output of the command:\n");
				String s = null;
				// int state =0;
				while ((s = stdInput.readLine()) != null) {
					System.out.println(s);
				}

			// read any errors from the attempted command
			System.out.println("Here is the standard error of the command (if any):\n");
			while ((s = stdError.readLine()) != null) {
				System.out.println(s);
				if (s.contains("QjackCtl")) {
					score++;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (score >= 1)isAvivable = true;
		}
		return isAvivable;
	}
	
	public static void main(String[] args) {
		System.out.println("Welcome to Jack<>Pulse version "+VERSION+", running initial enviorment check");
		File f = new File("/etc/debian_version");
		if(f.exists() && !f.isDirectory()) { 
		    isDebianBased=true;
		}
		pactlAndCmdAreAv=checkprocessavibility(PROCESS_TOCHECK_PACTL_AND_CMD);
		jackdIsAv   =    checkprocessavibility(PROCESS_TOCHECK_JACKD);
		qjackctlIsAv  =  checkprocessavibility(PROCESS_TOCHECK_QJACKCTL);
		pavucontrolIsAv =checkprocessavibility(PROCESS_TOCHECK_PAVUCONTROL);
		// get PID+hostname
		PID=ManagementFactory.getRuntimeMXBean().getName();
		{
			System.out.println("isDebianBased="+isDebianBased);
			System.out.println("pactlAndCmdAreAv="+pactlAndCmdAreAv);
			System.out.println("jackdIsAv="+jackdIsAv);
			System.out.println("qjackctlIsAv="+qjackctlIsAv);
			System.out.println("pavucontrolIsAv="+pavucontrolIsAv);
			System.out.println("PID="+PID);
		}
		
		
		//Tests:
		Sslist sinkslist = null;
		try {
			sinkslist=pacmd.gethardwaresinks();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Sslist sourceslist = null;
		try {
			sourceslist=pacmd.gethardwaresources();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("sinkslist:\n"+sinkslist);
		System.out.println("sourceslist:\n"+sourceslist);
	}

}
