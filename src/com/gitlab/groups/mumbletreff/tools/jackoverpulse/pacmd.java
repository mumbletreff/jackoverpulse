package com.gitlab.groups.mumbletreff.tools.jackoverpulse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class pacmd {
	
	
	protected static Sslist gethardwaresinks() throws IOException{
		Sslist sinkslist=new Sslist();
		Runtime rt = Runtime.getRuntime();
		String[] commands = {"pacmd","list-sinks"};
		System.out.println("Running "+"pacmd list-sinks"+":\n");

		Process proc = rt.exec(commands);

		BufferedReader stdInput = new BufferedReader(new 
		     InputStreamReader(proc.getInputStream()));

		BufferedReader stdError = new BufferedReader(new 
		     InputStreamReader(proc.getErrorStream()));

		// read the output from the command
		System.out.println("Here is the standard output of the command:\n");
		String s = null;
		String name = null,alsaname;
		//int state =0;
		while ((s = stdInput.readLine()) != null) {
		    System.out.println(s);
		    //if(state==0){//waiting for new sinkname (begin)
			    if(s.contains("name:")){
			    	String[] parts = s.split(":");
			    	parts[1].trim();
			    	//state =1;
			    	name=parts[1].trim();
			    	alsaname=null;
			    }
		    //}else if(state==1){//waiting for alsaname
			    if(s.contains("device.description = ")){

			    	String[] parts = s.split("\"");
			    	//parts[1].trim();
			    	//state =1;
			    	alsaname=parts[1];
			    	sinkslist.add(new SsItem(name, alsaname));
			    }
		    //}

		}

		// read any errors from the attempted command
		System.out.println("Here is the standard error of the command (if any):\n");
		while ((s = stdError.readLine()) != null) {
		    System.out.println(s);
		}
		return sinkslist;
	}
	
	protected static Sslist gethardwaresources() throws IOException{
		Sslist sourceslist=new Sslist();
		Runtime rt = Runtime.getRuntime();
		String[] commands = {"pacmd","list-sources"};
		System.out.println("Running "+"pacmd list-sources"+":\n");

		Process proc = rt.exec(commands);

		BufferedReader stdInput = new BufferedReader(new 
		     InputStreamReader(proc.getInputStream()));

		BufferedReader stdError = new BufferedReader(new 
		     InputStreamReader(proc.getErrorStream()));

		// read the output from the command
		System.out.println("Here is the standard output of the command:\n");
		String s = null;
		String name = null,alsaname;
		//int state =0;
		while ((s = stdInput.readLine()) != null) {
		    System.out.println(s);
		    //if(state==0){//waiting for new sinkname (begin)
			    if(s.contains("name:")){
			    	String[] parts = s.split(":");
			    	parts[1].trim();
			    	//state =1;
			    	name=parts[1].trim();
			    	alsaname=null;
			    }
		    //}else if(state==1){//waiting for alsaname
			    if(s.contains("device.description = ")){

			    	String[] parts = s.split("\"");
			    	//parts[1].trim();
			    	//state =1;
			    	alsaname=parts[1];
			    	sourceslist.add(new SsItem(name, alsaname));
			    }
		    //}

		}

		// read any errors from the attempted command
		System.out.println("Here is the standard error of the command (if any):\n");
		while ((s = stdError.readLine()) != null) {
		    System.out.println(s);
		}
		return sourceslist;
	}

}
