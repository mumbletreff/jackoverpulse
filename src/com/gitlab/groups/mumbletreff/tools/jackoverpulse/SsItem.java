package com.gitlab.groups.mumbletreff.tools.jackoverpulse;

import java.io.Serializable;

public class SsItem implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8512616484140088096L;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAlsacardname() {
		return description;
	}
	public void setAlsacardname(String alsacardname) {
		this.description = alsacardname;
	}
	String name;
	String description;
	SsItem(String name, String description){
		this.name=name;
		this.description=description;
	}
	SsItem(){}//null constructor
	
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "\""+name+"\",\""+description+"\"";
	}

}
