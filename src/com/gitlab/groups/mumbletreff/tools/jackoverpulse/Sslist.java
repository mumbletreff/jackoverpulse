package com.gitlab.groups.mumbletreff.tools.jackoverpulse;

import java.io.Serializable;
import java.util.ArrayList;

public class Sslist extends ArrayList<SsItem> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4723329246570009520L;
	
	
	public String toString() {
		// TODO Auto-generated method stub
		StringBuilder b;
		b=new StringBuilder();
		for (SsItem i : this) {
			b.append("{"+i+"}\n");
		}
		
		return b.toString();
	}


}
