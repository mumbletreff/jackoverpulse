package com.gitlab.groups.mumbletreff.tools.jackoverpulse;
//generated by widnow builder/swing designer eclipse plugin
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JFormattedTextField;
import javax.swing.JTextPane;
import java.awt.Dimension;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingConstants;
import javax.swing.JMenu;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JInternalFrame;
import javax.swing.JLayeredPane;
import javax.swing.JDesktopPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.JToggleButton;
import java.awt.Color;
import javax.swing.ListSelectionModel;

public class MainWindow extends JFrame {
	public static MainWindow mainWindow;

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//MainWindow frame = new MainWindow();
					mainWindow = new MainWindow();
					mainWindow.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		setSize(new Dimension(500, 320));
		setResizable(false);
		setTitle("Jack <> Pulse");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setBounds(100, 100, 500, 319);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File/Datei");
		menuBar.add(mnFile);
		
		JMenuItem mntmExitbeenden = new JMenuItem("Exit/Beenden");
		mntmExitbeenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mainWindow.dispose();
			}
		});
		mnFile.add(mntmExitbeenden);
		
		JMenu mnHelp = new JMenu("Help/Hilfe");
		mnHelp.setHorizontalAlignment(SwingConstants.TRAILING);
		menuBar.add(mnHelp);
		
		JMenuItem mntmBerabout = new JMenuItem("Über/About");
		mntmBerabout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(mainWindow, "JackOverPulse version "+Main.VERSION+"\n" +
						"for more information see/für mehr Informationen schaue unter:\n" +
						"http://gitlab.com/groups/mumbletreff/javaoverpulse/");
			}
		});
		mnHelp.add(mntmBerabout);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JTextPane txtpnInfoText = new JTextPane();
		txtpnInfoText.setEditable(false);
		txtpnInfoText.setText("<EN> This little tool is inteded to simple start Jackd as a \"pulseclient\"\n<DE> Dieses kleine Werkzeug ist dafür gedacht auf eine einfache Art jackd als ein \"Client für Pulseaudio\" zu starten");
		contentPane.add(txtpnInfoText, BorderLayout.NORTH);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		scrollPane.setViewportView(panel);
		panel.setLayout(null);
		
		JLabel lblNumberOfInputs = new JLabel("Number of Inputs/Anzahl der Eingänge:");
		lblNumberOfInputs.setBounds(12, 12, 291, 15);
		panel.add(lblNumberOfInputs);
		
		JSpinner spinnerNumberOfInputs = new JSpinner();
		spinnerNumberOfInputs.setModel(new SpinnerNumberModel(0, 0, 4, 1));
		spinnerNumberOfInputs.setBounds(327, 6, 60, 20);
		panel.add(spinnerNumberOfInputs);
		
		JLabel label = new JLabel("Number of Outputs/Anzahl der Außgänge:");
		label.setBounds(11, 34, 324, 15);
		panel.add(label);
		
		JSpinner spinnerNumberOfOutputs = new JSpinner();
		spinnerNumberOfOutputs.setModel(new SpinnerNumberModel(0, 0, 4, 1));
		spinnerNumberOfOutputs.setBounds(327, 32, 60, 20);
		panel.add(spinnerNumberOfOutputs);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(11, 64, 457, 130);
		panel.add(tabbedPane);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Connected Imputs/Outputs / Verbundene Ein- und Außgänge", null, panel_1, null);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{"Ausgang 1", null},
				{"Ausgang 2", null},
				{"Eingang 1", null},
			},
			new String[] {
				"Name", "Connected to"
			}
		));
//		scrollPane_1.add(table);
		
		JScrollPane scrollPane_1 = new JScrollPane(table);
		panel_1.add(scrollPane_1, BorderLayout.CENTER);
		

		
		JToggleButton tglbtnStartStop = new JToggleButton("Start/Stop");
		tglbtnStartStop.setBackground(Color.RED);
		tglbtnStartStop.setBounds(392, 6, 91, 53);
		panel.add(tglbtnStartStop);
	}
}
